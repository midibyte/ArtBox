
	</div>
</div>
<footer id="about">
	<!--
    <div class="container">
		<p class="to-top"><a href="#header">Back to top</a></p>
		<h2>About PirateBox</h2>
		<p>Inspired by pirate radio and the free culture movement, PirateBox is a self-contained mobile collaboration and file sharing device. PirateBox utilizes Free, Libre and Open Source software (FLOSS) to create mobile wireless file sharing networks where users can anonymously share images, video, audio, documents, and other digital content.</p>
		<p>PirateBox is designed to be safe and secure. No logins are required and no user data is logged. The system is purposely not connected to the Internet in order to prevent tracking and preserve user privacy.</p>
		<small>PirateBox is licensed under GPLv3.</small>
	</div>


<div class="container">
		<div class="card">
<h1 class="w3-margin-top w3-margin-bottom w3-xlarge">about</h1>
            
            -->
    <div class="container">
                <p>artbox is a portable, independent webserver. artbox is not connected to the web and stores all of its files locally.</p>
                <p>users can use artbox to chat and to share ideas and files on the fourms. users can post and upload files and remain completely anonymous.</p>
                <p>artbox was created by logan kelly. </p>
                <h1 class="w3-margin-top w3-large">attributions</h1>
                <p>artbox was derived from the PirateBox project created by Matthias Strubel and is licenced under the GNU General Public License 3.0. More info at piratebox.cc</p>
                <p>the flying toaster background was created by github user bryanbraun and is a replica of windows 98 screensavers created by berkley systems inc.</p>
            
            </div>
            </footer>